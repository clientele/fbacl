"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function fbacl(firestore) {
    var query = firestore.collection('settings').doc('icimod_api');
    var settings = {};
    query.onSnapshot(function (snap) {
        settings = snap.data();
    });
    return function (req, res, next) {
        if (settings.encrypt) {
            res.api_key = settings.api_key;
            next();
        }
    };
}
exports.fbacl = fbacl;
