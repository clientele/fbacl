export function fbacl(firestore:any) {
    const query = firestore.collection('settings').doc('icimod_api');
    let settings = <any>{};
    query.onSnapshot((snap:any) => {
  
      settings = snap.data();
     
    });
    return function(req:any, res:any, next:any) {
      if (settings.encrypt) {
        res.api_key = settings.api_key;
        next();
      }
  
  
    };
  }
  